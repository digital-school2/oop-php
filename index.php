<?php
 require_once 'animal.php';
 require_once 'frog.php';
 require_once 'ape.php';

 $sheep = new Animal("shaun");
 $sungokong = new ape("Kera Sakti");
 $kodok = new frog("buduk");
 
  echo "Name: " . $sheep->name . "<br>";
  echo "legs: " . $sheep->legs . "<br>";
  echo "cold blooded: " . $sheep->cold_blooded;
 echo "<hr>";
 echo "Name: " . $kodok->name . "<br>";
 echo "legs: " . $kodok->legs . "<br>";
 echo "cold blooded: " . $kodok->cold_blooded ."<br>";
 echo "jump: " . $kodok->jump() . "<br>";
 echo "<hr>";
 echo "Name: " . $sungokong->name . "<br>";
 echo "legs: " . $sungokong->legs . "<br>";
 echo "cold blooded: " . $sungokong->cold_blooded . "<br>";
 echo "yell: " . $sungokong->yell();